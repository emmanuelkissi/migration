# -*- coding: utf-8 -*-

import csv, os, sys, logging
from datetime import datetime
from tools import * 

def generate_stock_inventory(inventory_file, stock_inventory_line):
    fieldnames = ["product_id","location_id","product_qty"]
    generate_csv(inventory_file, fieldnames, stock_inventory_line)
    print u"Generation du fichier d'import inventaire article stock > 0 réussi"

def generate_product_import(product_file, load_product):
    fieldnames = ["default_code","name","categ_id","subcateg_id","barcode","description",
        "weight","sale_ok","purchase_ok","price", "standard_price", "purchase_price","type","list_price"]
    generate_csv(product_file, fieldnames, load_product)
    print u"Generation du fichier d'import article trié réussi"

def generate_pricelist_import(pricelist_file, load_pricelist):
    fieldnames = ["product_tmpl_id", "pricelist_id", "fixed_price", "min_quantity"]
    generate_csv(pricelist_file, fieldnames, load_pricelist)
    print u"Generation du fichier d'import liste de prix trié réussi"

def generate_article_error(error_file, load_error):
    product_header = ["default_code","name","categ_id","subcateg_id","barcode","description",
        "weight","sale_ok","purchase_ok","price", "standard_price", "purchase_price","type","list_price"]
    generate_csv(error_file, product_header, load_error)

def generate_stock_error(error_file, load_error):
    stock_header = ["product_id", "designation", "famille", "product_qty"]
    generate_csv(error_file, stock_header, load_error)

def generate_product(brute_file, export_dir, error_dir):
    row_sheet = 1
    load_product = []
    stock_inventory_line = []
    load_pricelist = []
    no_name = []
    no_family = []
    no_stock = []
    no_price = []
    barcode_doublon = []
    barcodelist = []
    with open(brute_file) as csvfile:
        reader = csv.DictReader(csvfile, delimiter=';')
        for row in reader:
            tarif_un = convert_float(row["Prix de VenteTTC (1)"])
            product = {
                    'default_code': row["Code"],
                    'name': row["Designation"],
                    'categ_id': row["Code Famille"],
                    'subcateg_id': row["Code Famille"],
                    'type':'product',
                    'sale_ok':'True',
                    'purchase_ok':'True',
                    'purchase_price': convert_float(row["Prix d'achat"]),
                    'standard_price': convert_float(row["Prix de revient"]),
                    'weight': convert_float(row["Poids brut"]),
                    'list_price':tarif_un,
                    }
            if row["Code Barre"] != '':
                product.update({'barcode':row["Code Barre"]})

            if row['Designation'] == '':    
                no_name.append(product)

            elif row['Code Famille'] == '':
                no_family.append(product)

            elif tarif_un <= 0:
                no_price.append(product)
            
            else:
                load_product.append(product)
                
                if tarif_un > 0:
                    load_pricelist.append({"product_tmpl_id":row["Code"],'pricelist_id':'product.list0',"fixed_price": tarif_un,"min_quantity":"1"})
                
                tarif_deux = convert_float(row["Prix de VenteTTC (2)"])
                if tarif_deux > 0:
                    load_pricelist.append({"product_tmpl_id":row["Code"],'pricelist_id':'maison_dewany_base.tarif_deux',"fixed_price": tarif_deux,"min_quantity":"1"})
                
                tarif_trois = convert_float(row["Prix de VenteTTC (5)"])
                if tarif_trois > 0:
                    load_pricelist.append({"product_tmpl_id":row["Code"],'pricelist_id':'maison_dewany_base.tarif_trois',"fixed_price": tarif_trois,"min_quantity":"1"})
                
                current_stock = convert_float(row["Stock Actuel (qte)"])
                if current_stock == 0:
                    pass
                elif current_stock > 0: 
                    stock_inventory_line.append({'product_id':row["Code"], 'location_id':'stock.stock_location_stock', 'product_qty': current_stock})
                elif current_stock < 0:
                    no_stock.append({'product_id':row["Code"], 'designation':row['Designation'], 'famille':row["Code Famille"], 'product_qty': current_stock})
            row_sheet = row_sheet + 1

    generate_product_import(export_dir + 'import-article-odoo.csv',load_product)
    generate_pricelist_import(export_dir + 'import-pricelist-odoo.csv',load_pricelist)
    generate_stock_inventory(export_dir + 'import-inventory-odoo.csv', stock_inventory_line)
    generate_article_error(error_dir + 'no_name.csv', no_name)
    generate_article_error(error_dir + 'no_family.csv', no_family)
    generate_article_error(error_dir + 'no_price.csv', no_price)
    generate_stock_error(error_dir + 'no_stock.csv', no_stock)