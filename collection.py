import csv, os, logging
from tools import *

def generate_collection_import(export_file, load_collection):
    fieldnames = ["code","name"]
    generate_csv(export_file, fieldnames, load_collection)

def generate_collection(brute_file, export_dir, error_dir):
    row_sheet = 1
    load_collection = []
    with open(brute_file) as csvfile:
        reader = csv.DictReader(csvfile, delimiter=';')
        for row in reader:
            collection = {
                'code': row["Code"],
                'name': row["Libelle"]
                }
            load_collection.append(collection)
            row_sheet = row_sheet + 1
    doublon = check_unicity('code', load_collection)
    if doublon == []:
        generate_collection_import(export_dir + 'import-collection-odoo.csv', load_collection)
        print ("Generation du fichier d'import collection réussi ")
    else:
        logging.error("Erreur de doublon detecté sur Collection")
        generate_collection_import(error_dir + 'collection_doublon.csv', doublon)