# -*- coding: utf-8 -*-
import re
import csv
def generate_product_variant_combination(brute_file,first_barcode,nmbre_line,import_product_product_file,import_a_va_file,first_tmpl_id):
    principale_ids = {'Bordeau':1, 'Brique':2, 'Argent Brillant':3, 'Rose Brillant':4, 'Blanc':5, 'Jaune':6, 'Rouge':7, 'Noir':8, 'Marron':9, 'Beige':10, 'Camel':11, 'Gris Foncé':12, 'Argent':13, 'Or':14, 'Rose':15, 'Saumon':16, 'Bleu':17, 'Bleu Foncé':18, 'Fuchsia':19, 'Turquoise':20, 'Mauve':21, 'Orange':22, 'Vert':23, 'Vert Olive':24, 'Gris blanc':25, 'Blanc gris':26, 'Bronze':27, 'Armée':28, 'noi':29, '41':30, '42':31, 'VERT':32, '19':33, 'br':34, 'bleu Fonce':35, 'Gris Fonce':36, '36':37, 'Bleu Jeans':38, '40':39, '43':40, 'rou':41}
    secondaire_ids = {'Bordeau':42, 'Blanc':43, 'Jaune':44, 'Rouge':45, 'Noir':46, 'Marron':47, 'Beige':48, 'Camel':49, 'Gris Foncé':50, 'Argent':51, 'Or':52, 'Rose':53, 'Saumon':54, 'Bleu':55, 'Bleu Foncé':56, 'Fuchsia':57, 'Turquoise':58, 'Mauve':59, 'Orange':60, 'Vert':61, 'Vert Olive':62, 'noi':63}
    pointure_ids ={'41':64, '42':65, '22':66, '19':67, '20':68, '21':69, '23':70, '24':71, '25':72, '26':73, '27':74, '28':75, '29':76, '30':77, '31':78, '32':79, '33':80, '34':81, '35':82, '36':83, '37':84, '38':85, '39':86, '40':87, '43':88, '44':89, '45':90, '46':91, '47':92, '48':93, '49':94, '50':95, '2930':96}
    adult_ids = {'small':97, 'medium':98, 'large':99, 'xl':100, 'xxl':101, 'xxxl':102}
    enfant_ids = {'1 an':103, '2 an':104, '3 an':105, '4 an':106, '5 an':107, '6 an':108, '7 an':109, '8 an':110, '9 an':111, '10 an':112, '11 an':113, '12 an':114, '13 an':115, '14 an':116, '15 an':117, '16 an':118}
    principale_value =[]
    secondaire_value = []
    pointure_value =[]
    adult_value = []
    enfant_value =[]
    principale = 'Couleur Principale'
    pointure = 'Pointure'
    secondaire= 'Couleur Secondaire'
    adult ='Taille Adult'
    enfant ='Taille enfant'
    compteur =0
    test = first_barcode[0:11]
    value_list=[]
    final_value_list=[]
    
   
    with open(brute_file) as csvfile:
        reader = csv.DictReader(csvfile, delimiter=',')     
        for row in reader: 
            barcode = row['barcode']
            compteur +=1
            if(barcode[0:11]==test and principale in row['product_attribute']):
                valeur = re.search('Principale: (.+?),Pointure', row['product_attribute']).group(1)
                if 'Couleur' in valeur:
                    valeur = re.search('Principale: (.+?),Couleur', row['product_attribute']).group(1)
                if(valeur not in principale_value):
                    principale_value.append(valeur)
            if(barcode[0:11]==test and secondaire in row['product_attribute']):
                valeur = re.search('Secondaire: (.+?),Pointure', row['product_attribute']).group(1)
                if(valeur not in secondaire_value):
                    secondaire_value.append(valeur)
            if(barcode[0:11]==test and pointure in row['product_attribute']):
                valeur = re.search('Pointure: (.+$)', row['product_attribute']).group(1)
                if(valeur not in pointure_value):
                    pointure_value.append(valeur)
            if(barcode[0:11]==test and adult in row['product_attribute']):
                valeur = re.search('): (.+$)', row['product_attribute']).group(1)
                if(valeur not in adult_value):
                    adult_value.append(valeur)
            if(barcode[0:11]==test and enfant in row['product_attribute']):
                valeur = re.search('ts): (.+$)', row['product_attribute']).group(1)
                if(valeur not in enfant_value):
                    enfant_value.append(valeur)
            
            if(barcode[0:11]==test):
                line_value={
                    "name":row['name'],
                    "principale":principale_value,   
                    "secondaire":secondaire_value,
                    "pointure":pointure_value,
                    "adult":adult_value,
                    "enfant":enfant_value,
                    "barcode":barcode        
                }
                value_list.append(line_value)
            if(barcode[0:11]!=test):
                value = value_list[-1]
                final_value_list.append(value)
                principale_value =[]
                secondaire_value = []
                pointure_value =[]
                adult_value = []
                enfant_value =[]
            if(barcode[0:11]!=test and principale in row['product_attribute']):
                valeur = re.search('Principale: (.+?),Pointure', row['product_attribute']).group(1)
                if 'Couleur' in valeur:
                    valeur = re.search('Principale: (.+?),Couleur', row['product_attribute']).group(1)
                if(valeur not in principale_value):
                    principale_value.append(valeur)
            if(barcode[0:11]!=test and secondaire in row['product_attribute']):
                valeur = re.search('Secondaire: (.+?),Pointure', row['product_attribute']).group(1)
                if(valeur not in secondaire_value):
                    secondaire_value.append(valeur)
            if(barcode[0:11]!=test and pointure in row['product_attribute']):
                valeur = re.search('Pointure: (.+$)', row['product_attribute']).group(1)
                if(valeur not in pointure_value):
                    pointure_value.append(valeur)
            if(barcode[0:11]!=test and adult in row['product_attribute']):
                valeur = re.search('): (.+$)', row['product_attribute']).group(1)
                if(valeur not in adult_value):
                    adult_value.append(valeur)
            if(barcode[0:11]!=test and enfant in row['product_attribute']):
                valeur = re.search('ts): (.+$)', row['product_attribute']).group(1)
                if(valeur not in enfant_value):
                    enfant_value.append(valeur)
            if(compteur == nmbre_line) :
                final_value_list.append(value_list[-1])          
            test = barcode[0:11]
        #print("final",final_value_list)
    product_template_attribute_values =[]
    variant_combinations =[]
    id_test = first_tmpl_id
    with open(import_a_va_file) as csvfile:
        reader = csv.DictReader(csvfile, delimiter=',')     
        for row in reader:
            attribute_value = {
                'id':row['id'],
                'product_attribute_value_id':row['product_attribute_value_id'],
                'attribute_id':row['attribute_id'],
                'product_tmpl_id':row['product_tmpl_id']
            }
            product_template_attribute_values.append(attribute_value)
    
    with open(import_product_product_file) as csvfile:
        l1=[]
        l2=[]
        compteur=0
        cmpteur1 =0
        reader = csv.DictReader(csvfile, delimiter=',')     
        for row in reader:
            values=[]
            if id_test != int(row['product_tmpl_id']):
                    l2 =[]  
                    print(f'-----avant--je vide---------',id_test) 
            for j in range(0,len(product_template_attribute_values)):
                attributes = product_template_attribute_values[j]
                
                if row['product_tmpl_id']==attributes['product_tmpl_id']:
                    l1.append(row['product_tmpl_id'])
                    compteur +=1
                    variant_comination = {
                        'product_product_id':int(row['id'])
                    }
                    
                    
                    if attributes['attribute_id']=='5' and 5 not in values and attributes['product_attribute_value_id'] not in l2:
                        variant_comination.update({'product_template_attribute_value_id':attributes['id']})
                        variant_combinations.append(variant_comination)
                        values.append(5)
                        l2.append(attributes['product_attribute_value_id'])
                    if attributes['attribute_id']=='6' and 6 not in values and attributes['product_attribute_value_id'] not in l2:
                        variant_comination.update({'product_template_attribute_value_id':attributes['id']})
                        variant_combinations.append(variant_comination)
                        values.append(6)
                        l2.append(attributes['product_attribute_value_id'])
                    if attributes['attribute_id']=='7' and 7 not in values and attributes['product_attribute_value_id'] not in l2:
                        variant_comination.update({'product_template_attribute_value_id':attributes['id']})
                        variant_combinations.append(variant_comination)
                        values.append(7)
                        l2.append(attributes['product_attribute_value_id'])
                    if attributes['attribute_id']=='8' and 8 not in values and attributes['product_attribute_value_id'] not in l2:
                        variant_comination.update({'product_template_attribute_value_id':attributes['id']})
                        variant_combinations.append(variant_comination)
                        values.append(8)
                        l2.append(attributes['product_attribute_value_id'])
                    if attributes['attribute_id']=='9' and 9 not in values and attributes['product_attribute_value_id'] not in l2:
                        variant_comination.update({'product_template_attribute_value_id':attributes['id']})
                        variant_combinations.append(variant_comination)
                        values.append(9)
                        l2.append(attributes['product_attribute_value_id'])   
            id_test=row['product_tmpl_id']   
            print("----------------------------------------------apres",id_test)  
                
    #print(variant_combinations)
    labels =['product_product_id','product_template_attribute_value_id']
    with open('import/import_product_variant_combination.csv', 'w') as f:
        writer = csv.DictWriter(f, fieldnames=labels)
        writer.writeheader()
        for elem in variant_combinations:
            writer.writerow(elem) 
    print("fin de generation file csv")                   
                    
  
            
generate_product_variant_combination('product_product_test.csv','20210500717394441',70,"import/import_product_product.csv","import/import_product_template_attribute_value.csv",3)
