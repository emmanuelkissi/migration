# -*- coding: utf-8 -*-
import csv
import psycopg2
import re


def get_product_id_barcode_tmpl_id():
    try:
        conn = psycopg2.connect(
            user = "odoo",
            password = "odoo",
            host = "160.155.205.29",
            port = "5432",
            database = "migration-test"
        )
        cur = conn.cursor()

        sql = "SELECT id,barcode,product_tmpl_id  FROM product_product WHERE id>2"

        cur.execute(sql)
        print("Sélectionner des lignes dans la table product_product")
        res = cur.fetchall() 
        fileName = ["id","barcode",'product_tmpl_id']
        with open("migration-test1/product_id_barcode_ecommerce.csv", 'w') as csvfile:
            writer = csv.writer(csvfile)
            writer.writerow(fileName)
            for row in res:
                print("Id = ", row[0], )
                writer.writerow([row[0],row[1],row[2]])
    
        #fermeture de la connexion à la base de données
        cur.close()
        conn.close()
        print("La connexion PostgreSQL est fermée")

    except (Exception, psycopg2.Error) as error :
        print ("Erreur lors du sélection à partir de la table", error)

def generate_product_public_category_product_template(product_template_file,ecommerce_category_file):
    ecommerce_category = []
    category_template =[]
    with open(ecommerce_category_file) as csvfile:
        reader = csv.DictReader(csvfile, delimiter=',')     
        for row in reader:
            ecommerce={
                'id':row['id'],#id de la bd
                'name':row['name']
            }
            ecommerce_category.append(ecommerce)
    with open(product_template_file) as csvfile:
        reader = csv.DictReader(csvfile, delimiter=',')     
        for row in reader:
            categ_ecomme = row['public_categ_ids']
            categ1 =re.search('(^.+?),', categ_ecomme).group(1)
            categ2 = re.search(',(.+$)', categ_ecomme).group(1)
            for i in range(0,len(ecommerce_category)):
                category = ecommerce_category[i]
                if categ1==category['name']:
                    categ_tmpl ={
                        'name':row['name'],
                        'sequence':row['product_seq'],
                        'public_categ_id':category['id']
                    }
                    category_template.append(categ_tmpl)
                if categ2==category['name']:
                    categ_tmpl ={
                        'name':row['name'],
                        'sequence':row['product_seq'],
                        'public_categ_id':category['id']
                    }
                    category_template.append(categ_tmpl)
    labels =['name','sequence','public_categ_id']
    with open('import/product_public_category_product_template.csv', 'w') as f:
        writer = csv.DictWriter(f, fieldnames=labels)
        writer.writeheader()
        for elem in category_template:
            writer.writerow(elem) 
            
    return

def generate_import_product_public_category_product_template_rel(product_public_category_product_template_file,product_id_barcode_ecommerce_file):
    barcodes_ecommerces=[]
    category_template =[]
    tmpl_ids =[]
    with open(product_id_barcode_ecommerce_file) as csvfile:
        reader = csv.DictReader(csvfile, delimiter=',')     
        for row in reader:
            barcode_ecommerce ={
               'id':row['id'],
               'barcode':row['barcode'],
               'product_tmpl_id':row['product_tmpl_id']
            } 
            barcodes_ecommerces.append(barcode_ecommerce)
    with open(product_public_category_product_template_file) as csvfile:
        reader = csv.DictReader(csvfile, delimiter=',')     
        for row in reader:
            sequence = row['sequence']
            for i in range(0,len(barcodes_ecommerces)):
                product_info = barcodes_ecommerces[i]
                barcode = product_info['barcode']
                tmpi_id = product_info['product_tmpl_id']
                if sequence==barcode[6:11] and tmpi_id not in tmpl_ids:
                    category = {
                        'product_public_category_id':row['public_categ_id'],
                        'product_template_id':tmpi_id
                    }
                    category_template.append(category)
                    tmpl_ids.append(tmpi_id)
    labels =['product_public_category_id','product_template_id']
    with open('import/import_product_public_category_product_template_rel.csv', 'w') as f:
        writer = csv.DictWriter(f, fieldnames=labels)
        writer.writeheader()
        for elem in category_template:
            writer.writerow(elem)         
            	
            
    return
#generate_product_public_category_product_template("migration-test1/product_template_ecommerce.csv","migration-test1/ecommerce_category.csv")   
get_product_id_barcode_tmpl_id()
#generate_import_product_public_category_product_template_rel('import/product_public_category_product_template.csv',"migration-test1/product_product.csv")
