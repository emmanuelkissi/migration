import psycopg2
from openpyxl import Workbook 
import csv 

try:
    conn = psycopg2.connect(
          user = "odoo",
          password = "12345678",
          host = "localhost",
          port = "5432",
          database = "migration-test4"
    )
    cur = conn.cursor()

    sql = "SELECT id  FROM product_product"

    cur.execute(sql)
    print("Sélectionner des lignes dans la table template")
    res = cur.fetchall() 
    fileName = ["id"]
    with open("product_product_id.csv", 'w') as csvfile:
        writer = csv.writer(csvfile)
        writer.writerow(fileName)
        for row in res:
            print("Id = ", row[0], )
            writer.writerow([row[0]])
  
    #fermeture de la connexion à la base de données
    cur.close()
    conn.close()
    print("La connexion PostgreSQL est fermée")

except (Exception, psycopg2.Error) as error :
    print ("Erreur lors du sélection à partir de la table person", error)

