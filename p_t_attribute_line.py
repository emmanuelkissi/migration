# -*- coding: utf-8 -*-
import csv 
from datetime import datetime
import psycopg2
#genere le file import de p_t_attribute_line
def generate_attribute_line_file(brute_file,first_barcode,nmbre_line):
    test = first_barcode[0:11]
    principale = 'Couleur Principale'
    pointure = 'Pointure'
    secondaire= 'Couleur Secondaire'
    adult ='Taille Adult'
    enfant ='Taille enfant'
    attribute_id =[]
    attribute_list=[]
    final_attribute_list=[]
    attribute_compte =[]
    principale_nombre,pointure_nombre,secondaire_nombre,adult_nombre,enfant_nombre=0,0,0,0,0

    compteur =0
    choix = "ok"
    with open(brute_file) as csvfile:
        reader = csv.DictReader(csvfile, delimiter=',')     
        for row in reader:
            attribute_id=[]
            barcode = row['barcode']
            compteur +=1
            #print("avant----------------",final_attribute_list)
            #à chaque fois on ajoute les id des differente caracteristiques
            if(barcode[0:11]==test and principale in row['product_attribute']):
                attribute_id.append(2)
            if(barcode[0:11]==test and secondaire in row['product_attribute']):
                attribute_id.append(3)
            if(barcode[0:11]==test and pointure in row['product_attribute']):
                attribute_id.append(4)
            if(barcode[0:11]==test and adult in row['product_attribute']):
                attribute_id.append(5)
            if(barcode[0:11]==test and enfant in row['product_attribute']):
                attribute_id.append(6)
            if(barcode[0:11]==test):
                line_value={
                    "name":row['name'],
                    "attribute_id":attribute_id,
                    "barcode" :barcode        
                }
                attribute_list.append(line_value)
                #recuperation du dernier element contenant l'ensembles de caracteristique et enregistrement 
                #du nombre d'occurence de chaque caracteristique
            if(barcode[0:11]!=test):
                ids = attribute_list[-1]
                final_attribute_list.append(ids)
                
            if(barcode[0:11]!=test and principale in row['product_attribute']):
                attribute_id.append(2)
            if(barcode[0:11]!=test and secondaire in row['product_attribute']):
                attribute_id.append(3)
            if(barcode[0:11]!=test and pointure in row['product_attribute']):
                attribute_id.append(4)
            if(barcode[0:11]!=test and adult in row['product_attribute']):
                attribute_id.append(5)
            if(barcode[0:11]!=test and enfant in row['product_attribute']):
                attribute_id.append(6)
            #ajout du premier element
            
            if(compteur == nmbre_line):
                ids = attribute_list[-1]
                final_attribute_list.append(ids)
                print("fin des ligne")
            test = barcode[0:11]
            choix =""
            
     #tentative de resolution du comptage du nombre de chaque caracteristique   
        print(compteur)
    """
    pr_id,po_id,se_id,ad_id,en_id=0,0,0,0,0
    for val in attribute_list:
        barcod = val['barcode']
        for val1 in attribute_list:
            if barcod[6:11] == val1['barcode'][6:11]:
                principale_nombre += val1['attribute_id'].count(pr_id)
                pointure_nombre += val1['attribute_id'].count(po_id)
                secondaire_nombre += val1['attribute_id'].count(se_id)
                adult_nombre += val1['attribute_id'].count(ad_id)
                enfant_nombre +=val1['attribute_id'].count(en_id)
            else:
                compte ={
                    'compte_pr':principale_nombre,
                    'compte_po':pointure_nombre,
                    'compte_se':secondaire_nombre,
                    'compte_ad':adult_nombre,
                    'compte_en':enfant_nombre
                }
                attribute_compte.append(compte)
                principale_nombre,pointure_nombre,secondaire_nombre,adult_nombre,enfant_nombre=1,1,1,1,1
            """

    list_line=[]
    print('---------------------------attribute id',attribute_id)
    print("******************",final_attribute_list)
    
    id=1
    #parcours de la liste contenat les ids attribute et nom des article
    for i in range(0,len(final_attribute_list)):
        dicte = final_attribute_list[i]
        attribute = dicte['attribute_id']
        code = dicte['barcode']
        for j in range(0,len(attribute)):
            eleme ={
                'attribute_id':attribute[j],
                'name':dicte['name'],
                'sequence':code[6:11]
            }
            list_line.append(eleme)
            id +=1
            
        print(attribute)
    labels =['attribute_id','name','sequence']
    with open('import/product_template_attribute_line.csv', 'w') as f:
        writer = csv.DictWriter(f, fieldnames=labels)
        writer.writeheader()
        for elem in list_line:
            writer.writerow(elem) 
    
    return None

def get_product_info():
    try:
        conn = psycopg2.connect(
            user = "odoo",
            password = "12345678",
            host = "localhost",
            port = "5432",
            database = "migration-test4"
        )
        cur = conn.cursor()

        sql = "SELECT id,product_tmpl_id,barcode  FROM product_product"

        cur.execute(sql)
        print("Sélectionner des lignes dans la table template")
        res = cur.fetchall() 
        fileName = ["id",'product_tmpl_id','barcode']
        with open("file/product_product_info.csv", 'w') as csvfile:
            writer = csv.writer(csvfile)
            writer.writerow(fileName)
            for row in res:
                print("Id = ", row[0], )
                writer.writerow([row[0],row[1],row[2]])
    
        #fermeture de la connexion à la base de données
        cur.close()
        conn.close()
        print("La connexion PostgreSQL est fermée")

    except (Exception, psycopg2.Error) as error :
        print ("Erreur lors du sélection à partir de la table person", error)
        

def generate_import_attribute_line_file(attribute_line_file,product_template_file,first_sequence):
    product_info=[]
    attribute_line=[]
    attribute_id=[]
    product_seq=[]
    product_seq.append(first_sequence)
    #etape8
    attribute_rel=[]
    
    with open(product_template_file) as csvfile:
        reader = csv.DictReader(csvfile, delimiter=',')     
        for row in reader:
            product = {
                'product_tmpl_id':row['id'],
                'product_seq':row['product_seq']
            }
            product_info.append(product)
    print('----------',product_info)
    with open(attribute_line_file) as csvfile:
        reader = csv.DictReader(csvfile, delimiter=',')
        
        for row in reader:
            sequence=row['sequence']
            if sequence!=product_seq[-1]:
                attribute_id =[]
            for i in range(0,len(product_info)):
                info= product_info[i] 
                #barcode = info['barcode']  
                if sequence==info['product_seq'] and row['attribute_id'] not in attribute_id:
                    attribute_id.append(row['attribute_id'])
                    print("bonjour")
                    eleme ={
                        'attribute_id':row['attribute_id'],
                        'product_tmpl_id':info['product_tmpl_id'],
                        'active':'TRUE',
                        'create_uid':2,
                        'create_date':datetime.now(),
                        'write_uid':2,
                        'write_date':datetime.now()
                    }
                    rel ={
                        'product_attribute_id':row['attribute_id'],
                        'product_template_id':info['product_tmpl_id']
                    }
                    attribute_line.append(eleme)
                    attribute_rel.append(rel)
            product_seq.append(sequence)
    labels =['attribute_id','product_tmpl_id','active','create_uid','create_date','write_uid','write_date']
    with open('import/import_product_template_attribute_line.csv', 'w') as f:
        writer = csv.DictWriter(f, fieldnames=labels)
        writer.writeheader()
        for elem in attribute_line:
            writer.writerow(elem)
    labels =['product_attribute_id','product_template_id']
    with open('import/import_product_attribute_product_template_rel.csv', 'w') as f:
        writer = csv.DictWriter(f, fieldnames=labels)
        writer.writeheader()
        for elem in attribute_rel:
            writer.writerow(elem)
    print("----------fin de import ----------------") 
    return
    

    

#generate_attribute_line_file('test_o15_2/product_product.csv', '20210500739410542', 60)
#get_product_info()
generate_import_attribute_line_file('import/product_template_attribute_line.csv','test_o15_2/product_template.csv','00717')


