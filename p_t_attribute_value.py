# -*- coding: utf-8 -*-
import csv
import re

def generate_product_attribute_value(brute_file,first_barcode):
    principale_values =  ['Bordeau', 'Brique', 'Argent Brillant', 'Rose Brillant', 'Blanc', 'Jaune', 'Rouge', 'Noir', 'Marron', 'Beige', 'Camel', 'Gris Foncé', 'Argent', 'Or', 'Rose', 'Saumon', 'Bleu', 'Bleu Foncé', 'Fuchsia', 'Turquoise', 'Mauve', 'Orange', 'Vert', 'Vert Olive', 'Gris blanc', 'Blanc gris', 'Bronze', 'Armée', 'noi', '41', '42', 'VERT', '19', 'br', 'bleu Fonce', 'Gris Fonce', '36', 'Bleu Jeans', '40', '43', 'rou']
    secondaire_values = ['Bordeau', 'Blanc', 'Jaune', 'Rouge', 'Noir', 'Marron', 'Beige', 'Camel', 'Gris Foncé', 'Argent', 'Or', 'Rose', 'Saumon', 'Bleu', 'Bleu Foncé', 'Fuchsia', 'Turquoise', 'Mauve', 'Orange', 'Vert', 'Vert Olive', 'noi']
    pointure_values =['41', '42', '22', '19', '20', '21', '23', '24', '25', '26', '27', '28', '29', '30', '31', '32', '33', '34', '35', '36', '37', '38', '39', '40', '43', '44', '45', '46', '47', '48', '49', '50', '2930']
    adult_values = ['small', 'medium', 'large', 'xl', 'xxl', 'xxxl']
    enfant_values =  ['1 an', '2 an', '3 an', '4 an', '5 an', '6 an', '7 an', '8 an', '9 an', '10 an', '11 an', '12 an', '13 an', '14 an', '15 an', '16 an']
    principale_value =[]
    secondaire_value = []
    pointure_value =[]
    adult_value = []
    enfant_value =[]
    principale = 'Couleur Principale'
    pointure = 'Pointure'
    secondaire= 'Couleur Secondaire'
    adult ='Taille Adult'
    enfant ='Taille enfant'
    compteur =0
    test = first_barcode[0:11]
    value_list=[]
    final_value_list=[]
    first="ok"
    """ 
    with open(value_brute) as csvfile:
        reader = csv.DictReader(csvfile, delimiter=',')     
        for row in reader:
            if 0<= compteur <=40:
                principale_values.append(row['name'])
            if 40<compteur<=62:
                secondaire_values.append(row['name'])
            if 62<compteur<96 :
                pointure_values.append(row['name'])
            if 96<= compteur <=101:
                adult_values.append(row['name'])
            if compteur > 101:
                enfant_values.append(row['name'])
            compteur +=1
        print("principale value: \n",enfant_values)   
     """ 
       
    with open(brute_file) as csvfile:
        reader = csv.DictReader(csvfile, delimiter=',')     
        for row in reader: 
            barcode = row['barcode']
            if(barcode[0:11]==test and principale in row['product_attribute']):
                for i in range(0,len(principale_values)):
                    if principale_values[i] in row['product_attribute']:
                        principale_value.append(principale_values[i])
            if(barcode[0:11]==test and secondaire in row['product_attribute']):
                for i in range(0,len(secondaire_values)):
                    if secondaire_values[i] in row['product_attribute']:
                        secondaire_value.append(secondaire_values[i])
            if(barcode[0:11]==test and pointure in row['product_attribute']):
                for i in range(0,len(pointure_values)):
                    if pointure_values[i] in row['product_attribute']:
                        pointure_value.append(pointure_values[i])
            if(barcode[0:11]==test and adult in row['product_attribute']):
                for i in range(0,len(adult_values)):
                    if adult_values[i] in row['product_attribute']:
                        adult_value.append(adult_values[i])
            if(barcode[0:11]==test and enfant in row['product_attribute']):
                for i in range(0,len(enfant_values)):
                    if enfant_values[i] in row['product_attribute']:
                        enfant_value.append(enfant_values[i])
            line_value={
                "name":row['name'],
                "principale":principale_value,   
                "secondaire":secondaire_value,
                "pointure":pointure_value,
                "adult":adult_value,
                "enfant":enfant_value        
            }
            value_list.append(line_value)
            print("value list",value_list)
            if(barcode[0:11]!=test and principale in row['product_attribute']):
                for i in range(0,len(principale_values)):
                    if principale_values[i] in row['product_attribute']:
                        principale_value.append(principale_values[i])
            if(barcode[0:11]!=test and secondaire in row['product_attribute']):
                for i in range(0,len(secondaire_values)):
                    if secondaire_values[i] in row['product_attribute']:
                        secondaire_value.append(secondaire_values[i])
            if(barcode[0:11]!=test and pointure in row['product_attribute']):
                for i in range(0,len(pointure_values)):
                    if pointure_values[i] in row['product_attribute']:
                        pointure_value.append(pointure_values[i])
            if(barcode[0:11]!=test and adult in row['product_attribute']):
                for i in range(0,len(adult_values)):
                    if adult_values[i] in row['product_attribute']:
                        adult_value.append(adult_values[i])
            if(barcode[0:11]!=test and enfant in row['product_attribute']):
                for i in range(0,len(enfant_values)):
                    if enfant_values[i] in row['product_attribute']:
                        enfant_value.append(enfant_values[i])
                        
            if(barcode[0:11]!=test and first in "ok"):
                final_value_list.append(value_list[-2])
                print("-2",value_list[-2])
            if(barcode[0:11]!=test):
                ids = value_list[-1]
                final_value_list.append(ids)
                principale_value =[]
                secondaire_value = []
                pointure_value =[]
                adult_value = []
                enfant_value =[]
            test = barcode[0:11]
                                   
        print("testtest",final_value_list) 
    return None
generate_product_attribute_value('attribute_test.csv','20210500717394441')

                