# -*- coding: utf-8 -*-
import re
import csv
from datetime import datetime

def generate_product_attribute_value(brute_file,first_barcode,nmbre_line,attribute_line_import_file,product_template_file):
    #principale_ids = {'Bordeau':552, 'Brique':553, 'Argent Brillant':554, 'Rose Brillant':555, 'Blanc':556, 'Jaune':557, 'Rouge':558, 'Noir':559, 'Marron':560, 'Beige':561, 'Camel':562, 'Gris Foncé':563, 'Argent':564, 'Or':565, 'Rose':566, 'Saumon':567, 'Bleu':568, 'Bleu Foncé':569, 'Fuchsia':570, 'Turquoise':571, 'Mauve':572, 'Orange':573, 'Vert':574, 'Vert Olive':575, 'Gris blanc':576, 'Blanc gris':577, 'Bronze':578, 'Armée':579, 'noi':580, '41':581, '42':582, 'VERT':583, '19':584, 'br':585, 'bleu Fonce':586, 'Gris FoncÃ©':587, '36':588, 'Bleu Jeans':589, '40':590, '43':591, 'rou':592}
    #secondaire_ids = {'Bordeau':593, 'Blanc':594, 'Jaune':595, 'Rouge':596, 'Noir':597, 'Marron':598, 'Beige':599, 'Camel':600, 'Gris FoncÃ©':601, 'Argent':602, 'Or':603, 'Rose':604, 'Saumon':605, 'Bleu':606, 'Bleu Foncé':607, 'Fuchsia':608, 'Turquoise':609, 'Mauve':610, 'Orange':611, 'Vert':612, 'Vert Olive':613, 'noi':614}
    #pointure_ids ={'41':615, '42':616, '22':617, '19':618, '20':619, '21':620, '23':621, '24':622, '25':623, '26':624, '27':625, '28':626, '29':627, '30':628, '31':629, '32':630, '33':631, '34':632, '35':633, '36':634, '37':635, '38':636, '39':637, '40':638, '43':639, '44':640, '45':641, '46':642, '47':643, '48':644, '49':645, '50':646, '2930':647}
    #adult_ids = {'small':648, 'medium':649, 'large':650, 'xl':651, 'xxl':652, 'xxxl':653}
    #enfant_ids = {'1 an':654, '2 an':655, '3 an':656, '4 an':657, '5 an':658, '6 an':659, '7 an':660, '8 an':661, '9 an':662, '10 an':663, '11 an':664, '12 an':665, '13 an':666, '14 an':667, '15 an':668, '16 an':669}
    
    #principale_ids = {'Bordeau':1, 'Brique':2, 'Argent Brillant':3, 'Rose Brillant':4, 'Blanc':5, 'Jaune':6, 'Rouge':7, 'Noir':8, 'Marron':9, 'Beige':10, 'Camel':11, 'Gris FoncÃ©':12, 'Argent':13, 'Or':14, 'Rose':15, 'Saumon':16, 'Bleu':17, 'Bleu Foncé':18, 'Fuchsia':19, 'Turquoise':20, 'Mauve':21, 'Orange':22, 'Vert':23, 'Vert Olive':24, 'Gris blanc':25, 'Blanc gris':26, 'Bronze':27, 'Armée':28, 'noi':29, '41':30, '42':31, 'VERT':32, '19':33, 'br':34, 'bleu Fonce':35, 'Gris FoncÃ©':36, '36':37, 'Bleu Jeans':38, '40':39, '43':40, 'rou':41}
    #secondaire_ids = {'Bordeau':42, 'Blanc':43, 'Jaune':44, 'Rouge':45, 'Noir':46, 'Marron':47, 'Beige':48, 'Camel':49, 'Gris FoncÃ©':50, 'Argent':51, 'Or':52, 'Rose':53, 'Saumon':54, 'Bleu':55, 'Bleu Foncé':56, 'Fuchsia':57, 'Turquoise':58, 'Mauve':59, 'Orange':60, 'Vert':61, 'Vert Olive':62, 'noi':63}
    #pointure_ids ={'41':64, '42':65, '22':66, '19':67, '20':68, '21':69, '23':70, '24':71, '25':72, '26':73, '27':74, '28':75, '29':76, '30':77, '31':78, '32':79, '33':80, '34':81, '35':82, '36':83, '37':84, '38':85, '39':86, '40':87, '43':88, '44':89, '45':90, '46':91, '47':92, '48':93, '49':94, '50':95, '2930':96}
    #adult_ids = {'small':97, 'medium':98, 'large':99, 'xl':100, 'xxl':101, 'xxxl':102}
    #enfant_ids = {'1 an':103, '2 an':104, '3 an':105, '4 an':106, '5 an':107, '6 an':108, '7 an':109, '8 an':110, '9 an':111, '10 an':112, '11 an':113, '12 an':114, '13 an':115, '14 an':116, '15 an':117, '16 an':118}
    
    principale_ids = {'Bordeau':1, 'Brique':2, 'Argent Brillant':3, 'Rose Brillant':4, 'Blanc':5, 'Jaune':6, 'Rouge':7, 'Noir':8, 'Marron':9, 'Beige':10, 'Camel':11, 'Gris FoncÃ©':12, 'Argent':13, 'Or':14, 'Rose':15, 'Saumon':16, 'Bleu':17, 'Bleu Foncé':18, 'Fuchsia':19, 'Turquoise':20, 'Mauve':21, 'Orange':22, 'Vert':23, 'Vert Olive':24, 'Gris blanc':25, 'Blanc gris':26, 'Bronze':27, 'Armée':28, 'noi':29, '41':30, '42':31, 'VERT':32, '19':33, 'br':34, 'bleu Fonce':35, 'Gris FoncÃ©':36, '36':37, 'Bleu Jeans':38, '40':39, '43':40, 'rou':41}
    secondaire_ids = {'Bordeau':42, 'Blanc':43, 'Jaune':44, 'Rouge':45, 'Noir':46, 'Marron':47, 'Beige':48, 'Camel':49, 'Gris FoncÃ©':50, 'Argent':51, 'Or':52, 'Rose':53, 'Saumon':54, 'Bleu':55, 'Bleu Foncé':56, 'Fuchsia':57, 'Turquoise':58, 'Mauve':59, 'Orange':60, 'Vert':61, 'Vert Olive':62, 'noi':63}
    pointure_ids ={'41':64, '42':65, '22':66, '19':67, '20':68, '21':69, '23':70, '24':71, '25':72, '26':73, '27':74, '28':75, '29':76, '30':77, '31':78, '32':79, '33':80, '34':81, '35':82, '36':83, '37':84, '38':85, '39':86, '40':87, '43':88, '44':89, '45':90, '46':91, '47':92, '48':93, '49':94, '50':95, '2930':96}
    adult_ids = {'small':97, 'medium':98, 'large':99, 'xl':100, 'xxl':101, 'xxxl':102}
    enfant_ids = {'1 an':103, '2 an':104, '3 an':105, '4 an':106, '5 an':107, '6 an':108, '7 an':109, '8 an':110, '9 an':111, '10 an':112, '11 an':113, '12 an':114, '13 an':115, '14 an':116, '15 an':117, '16 an':118}
    
    #principale_ids = {'Bordeau':119, 'Brique':120, 'Argent Brillant':121, 'Rose Brillant':122, 'Blanc':123, 'Jaune':124, 'Rouge':125, 'Noir':126, 'Marron':127, 'Beige':128, 'Camel':129, 'Gris Foncé':130, 'Argent':131, 'Or':132, 'Rose':133, 'Saumon':134, 'Bleu':135, 'Bleu Foncé':136, 'Fuchsia':137, 'Turquoise':138, 'Mauve':139, 'Orange':140, 'Vert':141, 'Vert Olive':142, 'Gris blanc':143, 'Blanc gris':144, 'Bronze':145, 'Armée':146, 'noi':147, '41':148, '42':149, 'VERT':150, '19':151, 'br':152, 'bleu Fonce':153, 'Gris Fonce':154, '36':155, 'Bleu Jeans':156, '40':157, '43':158, 'rou':159}
    #secondaire_ids = {'Bordeau':160, 'Blanc':161, 'Jaune':162, 'Rouge':163, 'Noir':164, 'Marron':165, 'Beige':166, 'Camel':167, 'Gris Foncé':168, 'Argent':169, 'Or':170, 'Rose':171, 'Saumon':172, 'Bleu':173, 'Bleu Foncé':174, 'Fuchsia':175, 'Turquoise':176, 'Mauve':177, 'Orange':178, 'Vert':179, 'Vert Olive':180, 'noi':181}
    #pointure_ids ={'41':182, '42':183, '22':184, '19':185, '20':186, '21':187, '23':188, '24':189, '25':190, '26':191, '27':192, '28':193, '29':194, '30':195, '31':196, '32':197, '33':198, '34':199, '35':200, '36':201, '37':202, '38':203, '39':204, '40':205, '43':206, '44':207, '45':208, '46':209, '47':210, '48':211, '49':212, '50':213, '2930':214}
    #adult_ids = {'small':215, 'medium':216, 'large':217, 'xl':218, 'xxl':219, 'xxxl':220}
    #enfant_ids = {'1 an':221, '2 an':222, '3 an':223, '4 an':224, '5 an':225, '6 an':226, '7 an':227, '8 an':228, '9 an':229, '10 an':230, '11 an':231, '12 an':232, '13 an':233, '14 an':234, '15 an':235, '16 an':236}
    principale_value =[]
    secondaire_value = []
    pointure_value =[]
    adult_value = []
    enfant_value =[]
    principale = 'Couleur Principale'
    pointure = 'Pointure'
    secondaire= 'Couleur Secondaire'
    adult ='Taille Adult'
    enfant ='Taille enfant'
    compteur =0
    test = first_barcode[0:11]
    value_list=[]
    final_value_list=[]
    first="ok"
    print("-----------",first_barcode[6:11])
    #recuperation des differentes valeurs de caracteristique et ajout dans une liste globale puis recuperer
    #le dernier lorsque on passe a un autre article chapeau 
    #nb: le dernier element contient 
    with open(brute_file) as csvfile:
        reader = csv.DictReader(csvfile, delimiter=',')     
        for row in reader: 
            barcode = row['barcode']
            compteur +=1
            if(barcode[0:11]==test and principale in row['product_attribute']):
                valeur = re.search('Principale: (.+?),Pointure', row['product_attribute']).group(1)
                if 'Couleur' in valeur:
                    valeur = re.search('Principale: (.+?),Couleur', row['product_attribute']).group(1)
                if(valeur not in principale_value):
                    principale_value.append(valeur)
            if(barcode[0:11]==test and secondaire in row['product_attribute']):
                valeur = re.search('Secondaire: (.+?),Pointure', row['product_attribute']).group(1)
                if(valeur not in secondaire_value):
                    secondaire_value.append(valeur)
            if(barcode[0:11]==test and pointure in row['product_attribute']):
                valeur = re.search('Pointure: (.+$)', row['product_attribute']).group(1)
                if(valeur not in pointure_value):
                    pointure_value.append(valeur)
            if(barcode[0:11]==test and adult in row['product_attribute']):
                valeur = re.search('): (.+$)', row['product_attribute']).group(1)
                if(valeur not in adult_value):
                    adult_value.append(valeur)
            if(barcode[0:11]==test and enfant in row['product_attribute']):
                valeur = re.search('ts): (.+$)', row['product_attribute']).group(1)
                if(valeur not in enfant_value):
                    enfant_value.append(valeur)
            
            if(barcode[0:11]==test):
                line_value={
                    "name":row['name'],
                    "principale":principale_value,   
                    "secondaire":secondaire_value,
                    "pointure":pointure_value,
                    "adult":adult_value,
                    "enfant":enfant_value,
                    "barcode":barcode        
                }
                value_list.append(line_value)
            if(barcode[0:11]!=test):
                value = value_list[-1]
                final_value_list.append(value)
                principale_value =[]
                secondaire_value = []
                pointure_value =[]
                adult_value = []
                enfant_value =[]
            if(barcode[0:11]!=test and principale in row['product_attribute']):
                valeur = re.search('Principale: (.+?),Pointure', row['product_attribute']).group(1)
                if 'Couleur' in valeur:
                    valeur = re.search('Principale: (.+?),Couleur', row['product_attribute']).group(1)
                if(valeur not in principale_value):
                    principale_value.append(valeur)
            if(barcode[0:11]!=test and secondaire in row['product_attribute']):
                valeur = re.search('Secondaire: (.+?),Pointure', row['product_attribute']).group(1)
                if(valeur not in secondaire_value):
                    secondaire_value.append(valeur)
            if(barcode[0:11]!=test and pointure in row['product_attribute']):
                valeur = re.search('Pointure: (.+$)', row['product_attribute']).group(1)
                if(valeur not in pointure_value):
                    pointure_value.append(valeur)
            if(barcode[0:11]!=test and adult in row['product_attribute']):
                valeur = re.search('): (.+$)', row['product_attribute']).group(1)
                if(valeur not in adult_value):
                    adult_value.append(valeur)
            if(barcode[0:11]!=test and enfant in row['product_attribute']):
                valeur = re.search('ts): (.+$)', row['product_attribute']).group(1)
                if(valeur not in enfant_value):
                    enfant_value.append(valeur)
            if(compteur == nmbre_line) :
                final_value_list.append(value_list[-1])          
            test = barcode[0:11]
        print("final",final_value_list)
    list_line=[]
    barcode_tmpl_ids =[]
    product_a_v_product_t_a_line = []
    print('**************',final_value_list)
    for i in range(0,len(final_value_list)):
        dicte = final_value_list[i]
        c_principale = dicte['principale']
        c_secondaire = dicte['secondaire']
        c_pointure = dicte['pointure']
        c_adult = dicte['adult']
        c_enfant = dicte['enfant']
        code = dicte['barcode']
        for j in range(0,len(c_principale)):
            eleme ={
                'attribute_id':2,
                'product_attribute_value_id':principale_ids[c_principale[j]],
                'create_uid':2,
                'write_uid':2,
                'create_date':datetime.now(),
                'write_date':datetime.now(),
                'ptav_active':'TRUE',
                'price_extra':0.00
            }
            
            with open(attribute_line_import_file) as csvfile:
                reader = csv.DictReader(csvfile, delimiter=',')     
                for row in reader: 
                   
                    if row['attribute_id']=='2' and row['sequence']==code[6:11]:
                        eleme.update({"attribute_line_id":int(row['id'])})
                        product_a_v_product_t_a_l = {
                            'product_template_attribute_line_id':int(row['id']),
                            'product_attribute_value_id':principale_ids[c_principale[j]]
                        }                        
                        product_a_v_product_t_a_line.append(product_a_v_product_t_a_l)        
            with open(product_template_file) as csvfile:
                reader = csv.DictReader(csvfile, delimiter=',')    
                 
                for row in reader:  
                    print('code -------------', code[6:11])
                    print("sequen ---------",row['product_seq'])    
                    if row['name']==dicte['name'] and row['product_seq']==code[6:11]:
                        
                        eleme.update({"product_tmpl_id":int(row['id'])})
                        barcode_tmpl_id={
                            'barcode':code,
                            'product_tmpl_id':int(row['id'])
                        }
                        barcode_tmpl_ids.append(barcode_tmpl_id)
            list_line.append(eleme)
        for k in range(0,len(c_secondaire)):
            eleme ={
                'attribute_id':3,         
                'product_attribute_value_id':secondaire_ids[c_secondaire[k]],
                'create_uid':2,
                'write_uid':2,
                'create_date':datetime.now(),
                'write_date':datetime.now(),
                'ptav_active':'TRUE',
                'price_extra':0
            }
            with open(attribute_line_import_file) as csvfile:
                reader = csv.DictReader(csvfile, delimiter=',')     
                for row in reader:
                    if row['attribute_id']=='3' and row['sequence']==code[6:11]:
                        eleme.update({"attribute_line_id":int(row['id'])})
                        product_a_v_product_t_a_l = {
                            'product_template_attribute_line_id':row['id'],
                            'product_attribute_value_id':secondaire_ids[c_secondaire[k]]
                        }
                        product_a_v_product_t_a_line.append(product_a_v_product_t_a_l)
                            
            
            with open(product_template_file) as csvfile:
                reader = csv.DictReader(csvfile, delimiter=',')     
                for row in reader:      
                    if row['name']==dicte['name'] and row['product_seq']==code[6:11]:
                        
                        eleme.update({"product_tmpl_id":int(row['id'])})
                        barcode_tmpl_id={
                            'barcode':code,
                            'product_tmpl_id':int(row['id'])
                        }
                        barcode_tmpl_ids.append(barcode_tmpl_id)
            list_line.append(eleme)
        for t in range(0,len(c_pointure)):
            eleme ={
                'attribute_id':4,
                'product_attribute_value_id': pointure_ids[c_pointure[t]],
                'create_uid':2,
                'write_uid':2,
                'create_date':datetime.now(),
                'write_date':datetime.now(),
                'ptav_active':'TRUE',
                'price_extra':0
            }
            with open(attribute_line_import_file) as csvfile:
                reader = csv.DictReader(csvfile, delimiter=',')     
                for row in reader:
                    if row['attribute_id']=='4' and row['sequence']==code[6:11]:
                        eleme.update({"attribute_line_id":int(row['id'])})
                        
                        product_a_v_product_t_a_l = {
                            'product_template_attribute_line_id':row['id'],
                            'product_attribute_value_id':pointure_ids[c_pointure[t]]
                        }
                        product_a_v_product_t_a_line.append(product_a_v_product_t_a_l)
            with open(product_template_file) as csvfile:
                reader = csv.DictReader(csvfile, delimiter=',')     
                for row in reader:      
                    if row['name']==dicte['name'] and row['product_seq']==code[6:11]:
                        
                        eleme.update({"product_tmpl_id":int(row['id'])})
                        barcode_tmpl_id={
                            'barcode':code,
                            'product_tmpl_id':int(row['id'])
                        }
                        barcode_tmpl_ids.append(barcode_tmpl_id)
    
            list_line.append(eleme)
        for z in range(0,len(c_adult)):
            eleme ={
                'attribute_id':5,
                'product_attribute_value_id':adult_ids[c_adult[z]],
                'create_uid':2,
                'write_uid':2,
                'create_date':datetime.now(),
                'write_date':datetime.now(),
                'ptav_active':'TRUE',
                'price_extra':0
            }           
            with open(attribute_line_import_file) as csvfile:
                reader = csv.DictReader(csvfile, delimiter=',')     
                for row in reader:                   
                    if row['attribute_id']=='5' and row['sequence']==code[6:11]:
                        eleme.update({"attribute_line_id":int(row['id'])})
                        product_a_v_product_t_a_l = {
                            'product_template_attribute_line_id':row['id'],
                            'product_attribute_value_id':adult_ids[c_adult[z]]
                        }
                        product_a_v_product_t_a_line.append(product_a_v_product_t_a_l)
            with open(product_template_file) as csvfile:
                reader = csv.DictReader(csvfile, delimiter=',')     
                for row in reader:      
                    if row['name']==dicte['name'] and row['product_seq']==code[6:11]:
                        
                        eleme.update({"product_tmpl_id":int(row['id'])})
                        barcode_tmpl_id={
                            'barcode':code,
                            'product_tmpl_id':int(row['id'])
                        }
                        barcode_tmpl_ids.append(barcode_tmpl_id)
            
            list_line.append(eleme)
        for f in range(0,len(c_enfant)):
            eleme ={
                'attribute_id':6,
                'product_attribute_value_id':enfant_ids[c_enfant[f]],
                'create_uid':2,
                'write_uid':2,
                'create_date':datetime.now(),
                'write_date':datetime.now(),
                'ptav_active':'TRUE',
                'price_extra':0
            }
            with open(attribute_line_import_file) as csvfile:
                reader = csv.DictReader(csvfile, delimiter=',')     
                for row in reader:
                    if row['attribute_id']=='6' and row['sequence']==code[6:11]:
                        eleme.update({"attribute_line_id":int(row['id'])})
                        
                        product_a_v_product_t_a_l = {
                            'product_template_attribute_line_id':row['id'],
                            'product_attribute_value_id':enfant_ids[c_enfant[f]]
                        }
                        product_a_v_product_t_a_line.append(product_a_v_product_t_a_l)
           
            with open(product_template_file) as csvfile:
                reader = csv.DictReader(csvfile, delimiter=',')     
                for row in reader:      
                    if row['name']==dicte['name'] and row['product_seq']==code[6:11]:
                         
                        eleme.update({"product_tmpl_id":int(row['id'])})
                        barcode_tmpl_id={
                            'barcode':code,
                            'product_tmpl_id':int(row['id'])
                        }
                        barcode_tmpl_ids.append(barcode_tmpl_id)
            list_line.append(eleme)
    
    
    labels =['product_attribute_value_id','attribute_id','attribute_line_id','product_tmpl_id','create_uid','write_uid','create_date','write_date','ptav_active','price_extra']
    with open('import/import_product_template_attribute_value.csv', 'w') as f:
        writer = csv.DictWriter(f, fieldnames=labels)
        writer.writeheader()
        for elem in list_line:
            writer.writerow(elem)
    
    labels =['product_attribute_value_id','product_template_attribute_line_id']
    with open('import/import_product_a_v_product_t_a_line.csv', 'w') as f:
        writer = csv.DictWriter(f, fieldnames=labels)
        writer.writeheader()
        for elem in product_a_v_product_t_a_line:
            writer.writerow(elem)
    labels =['barcode','product_tmpl_id']
    with open('use_for_p_v_combination.csv', 'w') as f:
        writer = csv.DictWriter(f, fieldnames=labels)
        writer.writeheader()
        for elem in barcode_tmpl_ids:
            writer.writerow(elem)
    #print("liste line",list_line)
    
    print("tout est parfait !")
        
generate_product_attribute_value('test_o15_2/product_product.csv','20210500739410640',60,"import/product_template_attribute_line.csv","test_o15_2/product_template.csv")

#"import/product_template_attribute_line.csv" l id a l'interieur est id en bd