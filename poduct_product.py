# -*- coding: utf-8 -*-
import csv
import pandas as pd
from datetime import datetime
import psycopg2
#compte nombre de variant de chaque article
def generate_product(brute_file,nmbre_line,first_barcode):
    row_sheet = 0
    tes = first_barcode[0:11]
    load_product = []
    prefixe_barcode = []
    variant={}
    product_variant = []
    original_variant =[]
    counted = 0
    compteur=0
    barcodes=[]
    
    with open(brute_file) as csvfile:
        reader = csv.DictReader(csvfile, delimiter=',')
        
        for row in reader:
            barcode = row['barcode']
            codebare = {
                "id":row['id'],
                "barcode":barcode
            }
            barcodes.append(codebare)
            compteur +=1
            prefixe_barcode.append(barcode[0:11])
            if(barcode[0:11]==tes):
                counted +=1
                #print("------",barcode[0:11])
                variant = {
                    "id":row['id'],
                    "name":row['name'],
                    "variant":counted
                    }
                product_variant.append(variant)          
            else:
                valeur = product_variant[-1]
                original_variant.append(valeur)
                counted = 1
            if(compteur ==nmbre_line):
                valeur = product_variant[-1]
                original_variant.append(valeur)  
                  
            tes = barcode[0:11]
    
           
            row_sheet+=1
        print(compteur)
           
    labels =['id','name','variant']
    with open('import/product_variant.csv', 'w') as f:
        writer = csv.DictWriter(f, fieldnames=labels)
        writer.writeheader()
        for elem in original_variant:
            writer.writerow(elem)
    labels =['id','barcode']
    with open('barcode.csv', 'w') as f:
        writer = csv.DictWriter(f, fieldnames=labels)
        writer.writeheader()
        for elem in barcodes:
            writer.writerow(elem)
    f2 = pd.read_csv("barcode.csv")
    f2.to_excel("barcode.xlsx")
    print("------comptage, barcode fini----------------")

def generate_combination_variant_tmplecsv():
    """
    transformer le deux csv en excel puis ouvrie avec excel et copier la colonne
    """
    
    f1 = pd.read_csv("import/product_variant.csv")
    f2 = pd.read_csv("template_test1.csv")
    f1.to_excel("import/product_variant.xlsx")
    f2.to_excel("template_test1.xlsx")
    return;
def generate_template_variant(template_file,variant_file):
    with open(template_file) as csvfile:
        i = 4
        products =[]
        reader = csv.DictReader(csvfile, delimiter=',')     
        for row in reader:
            product ={
                'id':i,
                'name':row['name'],
                'product_seq':row['product_seq']
            }
            products.append(product)
            i +=1
    with open(variant_file) as csvfile:
        j = 0
        combines =[]
        reader = csv.DictReader(csvfile, delimiter=',')     
        for row in reader:
            producte = products[j]
            combine = {
                "name":producte['name'],
                "product_seq":producte['product_seq'],
                "variant":row['variant'],
                "id":producte['id']
            }
            combines.append(combine)
            j +=1
    labels =['id','name','product_seq','variant']
    with open('import/combination_var_tmpl.csv', 'w') as f:
        writer = csv.DictWriter(f, fieldnames=labels)
        writer.writeheader()
        for elem in combines:
            writer.writerow(elem)
    print("fait !")
    return

def get_product_tmpl_id():
    try:
        conn = psycopg2.connect(
            user = "odoo",
            password = "odoo",
            host = "127.0.0.1",
            port = "5432",
            database = "migration"
        )
        cur = conn.cursor()

        sql = "SELECT id FROM product_product WHERE id>2" #id du dernier element déja inséré

        cur.execute(sql)
        print("Sélectionner des lignes dans la table product_template")
        res = cur.fetchall() 
        fileName = ["id"]
        with open("test_o15/product_product_id.csv", 'w') as csvfile:
            writer = csv.writer(csvfile)
            writer.writerow(fileName)
            for row in res:
                print("Id = ", row[0], )
                writer.writerow([row[0]])
    
        #fermeture de la connexion à la base de données
        cur.close()
        conn.close()
        print("La connexion PostgreSQL est fermée")

    except (Exception, psycopg2.Error) as error :
        print ("Erreur lors du sélection à partir de la table person", error)

#genère un csv qui sera utilise pour la table product_product
def generate_import_product_product(combination_variant_tmple,product_extract_file):
    product_product = []
    barcodes =[]
    id = 1
    with open(product_extract_file) as file:
        reader = csv.DictReader(file, delimiter=',')      
        for row in reader:
            barcode = row['barcode']
            barcodes.append(barcode)
    print('************',len(barcodes))
    with open(combination_variant_tmple) as csvfile:
        reader = csv.DictReader(csvfile, delimiter=',')      
        for row in reader:
            nmbre = int(row['variant'])
            for i in range(0,nmbre):
                product = {
                    "product_tmpl_id":row['id'],
                    "active":'TRUE',
                    "create_uid":2,
                    "write_uid":2,
                    "barcode":barcodes[id-1],
                    'create_date':datetime.now(),
                    'write_date':datetime.now(),
                    'base_unit_count':0
                }
                product_product.append(product)
                id +=1
        
    labels =['product_tmpl_id','active','create_uid','write_uid','barcode','create_date','write_date','base_unit_count']
    with open('import/import_product_product.csv', 'w') as f:
        writer = csv.DictWriter(f, fieldnames=labels)
        writer.writeheader()
        for elem in product_product:
            writer.writerow(elem)
           
    f1 = pd.read_csv("product_product.csv") 
    f1.to_excel("product_product.xlsx") 
    print('------------import_product_product--------')             
    return

def add_product_product_id(product_id_file, import_product_product_file):
    ids=[] 
    product_product=[] 
    with open(product_id_file) as csvfile:
        reader = csv.DictReader(csvfile, delimiter=',')     
        for row in reader:
            id ={
                'id':row['id']
            }
            ids.append(id)
    with open(import_product_product_file) as csvfile:
        i = 0
        products =[]
        reader = csv.DictReader(csvfile, delimiter=',')     
        for row in reader:
            product = {
                "id":ids[i],
                "product_tmpl_id":row['product_tmpl_id'],
                "active":'TRUE',
                "create_uid":2,
                "write_uid":2,
                "barcode":row['barcode'],
                'create_date':datetime.now(),
                'write_date':datetime.now(),
                'base_unit_count':0
                }
            product_product.append(product)
    labels =['id','product_tmpl_id','active','create_uid','write_uid','barcode','create_date','write_date','base_unit_count']
    with open('import/product_product_with_id.csv', 'w') as f:
        writer = csv.DictWriter(f, fieldnames=labels)
        writer.writeheader()
        for elem in product_product:
            writer.writerow(elem)
    

#generate_product("test_o15_2/product_product.csv",60,"20210500739410542") 
#generate_combination_variant_tmplecsv()je n'ai pas utiliser
#generate_template_variant("test_o15_2/product_template.csv",'import/product_variant.csv')
generate_import_product_product("import/combination_var_tmpl.csv","test_o15_2/product_product.csv")
#get_product_tmpl_id()
#add_product_product_id('test_o15/product_product_id.csv', 'import/import_product_product.csv')          
    
