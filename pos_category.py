# -*- coding: utf-8 -*-
import csv
import psycopg2
from datetime import datetime
import re

def get_pos_category():
    try:
        conn = psycopg2.connect(
            user = "odoo",
            password = "12345678",
            host = "localhost",
            port = "5432",
            database = "migration-test4"
        )
        cur = conn.cursor()

        sql = "SELECT id,name  FROM pos_category"

        cur.execute(sql)
        print("Sélectionner des lignes dans la table template")
        res = cur.fetchall() 
        fileName = ["id","name"]
        with open("file/pos_category_file.csv", 'w') as csvfile:
            writer = csv.writer(csvfile)
            writer.writerow(fileName)
            for row in res:
                print("Id = ", row[0], )
                writer.writerow([row[0],row[1]])
    
        #fermeture de la connexion à la base de données
        cur.close()
        conn.close()
        print("La connexion PostgreSQL est fermée")

    except (Exception, psycopg2.Error) as error :
        print ("Erreur lors du sélection à partir de la table person", error)

def generate_product_template_pos_category(product_template_file,pos_category_file):
    pos_category =[]
    names = []
    with open(pos_category_file) as csvfile:
        reader = csv.DictReader(csvfile, delimiter=',')     
        for row in reader:
            pos ={
                "id":row['id'],
                "name":row['name']
            }
            names.append(row['name'])
            pos_category.append(pos)
    with open(product_template_file) as csvfile:
        product_templates=[]
        exist_name=[]
        j=0
        reader = csv.DictReader(csvfile, delimiter=',')   
        for row in reader:
            pos_categ  = row['pos_categ_id']
            print('bonjour ')
            for i in range(0,len(pos_category)):
                category =pos_category[i]
                if str(category['name'])==pos_categ:
                    print('***********************' )
                    template ={
                        'sequence':row['sequence'],
                        'name':row['name'],
                        'list_price':row['list_price'],
                        'uom_id':1,
                        'tracking':'none',
                        'type':'product',
                        'categ_id':1,
                        'uom_po_id':1,
                        'sale_ok':'TRUE',
                        'purchase_ok':'TRUE',
                        'active':'TRUE',
                        'create_uid':2,
                        'write_uid':2,
                        'sale_line_warn':'no-message',
                        'expense_policy':'no',
                        'invoice_policy':'delivery' ,
                        'volume':0,
                        'weight':0,
                        'has_configurable_attributes' :'TRUE' ,
                        'create_date':datetime.now(),
                        'write_date':datetime.now(),
                        'available_in_pos':'TRUE' ,
                        'pos_categ_id':category['id'],
                        'website_size_x':1,
                        'website_size_y':1,
                        'website_sequence':10000,
                        'is_published':'TRUE',
                        'detailed_type':'product',
                        'base_unit_count':0,
                        'allow_out_of_stock_order':'TRUE',
                        'available_threshold': 5  ,
                        'show_availability': 'TRUE'        
                    }
                    j +=1
                    product_templates.append(template)
                elif pos_categ not in names and pos_categ not in exist_name:
                    template ={
                        'sequence':row['sequence'],
                        'name':row['name'],
                        'list_price':row['list_price'],
                        'uom_id':1,
                        'tracking':'none',
                        'type':'product',
                        'categ_id':1,
                        'uom_po_id':1,
                        'sale_ok':'TRUE',
                        'purchase_ok':'TRUE',
                        'active':'TRUE',
                        'create_uid':2,
                        'write_uid':2,
                        'sale_line_warn':'no-message',
                        'expense_policy':'no',
                        'invoice_policy':'delivery' ,
                        'volume':0,
                        'weight':0,
                        'has_configurable_attributes' :'TRUE' ,
                        'create_date':datetime.now(),
                        'write_date':datetime.now(),
                        'available_in_pos':'TRUE' ,
                        'pos_categ_id':None,
                        'website_size_x':1,
                        'website_size_y':1,
                        'website_sequence':10000,
                        'is_published':'TRUE',
                        'detailed_type':'product',
                        'base_unit_count':0,
                        'allow_out_of_stock_order':'TRUE',
                        'available_threshold': 5  ,
                        'show_availability': 'TRUE'        
                    }
                    j +=1
                    product_templates.append(template)
                    exist_name.append(pos_categ)

        print("je suis present",j)
    labels =['sequence','name','list_price','uom_id','tracking','type','categ_id','uom_po_id','sale_ok',
             'purchase_ok','active','create_uid','write_uid','sale_line_warn',
             'expense_policy','invoice_policy','volume','weight','has_configurable_attributes',
             'create_date','write_date','available_in_pos','pos_categ_id','website_size_x','website_size_y','website_sequence',
             'is_published','detailed_type','base_unit_count','allow_out_of_stock_order','available_threshold','show_availability']
    with open('import/import_product_template.csv', 'w') as f:
        writer = csv.DictWriter(f, fieldnames=labels)
        writer.writeheader()
        for elem in product_templates:
            writer.writerow(elem)            
    print("import file generate !")
    return
    
generate_product_template_pos_category("test_o15_2/product_template.csv","test_o15_2/pos_category.csv")
#get_pos_category()