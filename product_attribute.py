# -*- coding: utf-8 -*-
import csv
from datetime import datetime
def generate_attribute(attribute_file):
    attributes=[]
    with open(attribute_file) as csvfile:
        reader = csv.DictReader(csvfile, delimiter=',')     
        for row in reader:
            attribute ={
                'name':row['name'],
                'sequence':row['sequence'],
                'create_variant':'always',
                'display_type':row['display_type'],
                'create_uid':2,
                'create_date':datetime.now(),
                'write_uid':2,
                'write_date':datetime.now(),
                'visibility':'visible'
            }
            #'write_date':datetime.now().strftime('%d/%m/%Y')
            attributes.append(attribute)
    labels =['name','sequence','create_variant','display_type','create_uid','create_date','write_uid','write_date','visibility']
    with open('import/import_product_attribute.csv', 'w') as f:
        writer = csv.DictWriter(f, fieldnames=labels)
        writer.writeheader()
        for elem in attributes:
            writer.writerow(elem)
    
def generate_attribute_value(attribute_value_file):
    attributes=[]
    with open(attribute_value_file) as csvfile:
        reader = csv.DictReader(csvfile, delimiter=',')     
        for row in reader:
            attribute ={
                'name':row['name'],
                'sequence':1,
                'attribute_id':row['attribute_id'],
                'create_uid':2,
                'create_date':datetime.now(),
                'write_uid':2,
                'write_date':datetime.now()
            }
            attributes.append(attribute)
    labels =['name','sequence','attribute_id','create_uid','create_date','write_uid','write_date']
    with open('import/import_product_attribute_value.csv', 'w', encoding='utf-8') as f:
        writer = csv.DictWriter(f, fieldnames=labels)
        writer.writeheader()
        for elem in attributes:
            writer.writerow(elem)

#generate_attribute("test_o15_2/product_attribute.csv")
generate_attribute_value("test_o15_2/product_attribute.csv")