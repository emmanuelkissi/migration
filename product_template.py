# -*- coding: utf-8 -*-
import csv
from datetime import datetime
def generate_product_template(brute_file):
    templates=[]
    with open(brute_file) as csvfile:
        reader = csv.DictReader(csvfile, delimiter=',')     
        for row in reader:
            template ={
                'sequence':row['sequence'],
                'name':row['name'],
                'list_price':row['list_price'],
                'uom_id':1,
                'tracking':'none',
                'type':'product',
                'categ_id':1,
                'uom_po_id':1,
                'sale_ok':'TRUE',
                'purchase_ok':'TRUE',
                'active':'TRUE',
                'create_uid':2,
                'write_uid':2,
                'purchase_method':'receive',
                'sale_line_warn':'no-message',
                'purchase_line_warn':'no-message',
                'expense_policy':'no',
                'invoice_policy':'delivery' ,
                'volume':0,
                'weight':0,
                'has_configurable_attributes' :'TRUE' ,
                'create_date':datetime.now(),
                'write_date':datetime.now(),
                'available_in_pos':'TRUE'                
            }
            templates.append(template)
    labels =['sequence','name','list_price','uom_id','tracking','type','categ_id','uom_po_id','sale_ok',
             'purchase_ok','active','create_uid','write_uid','purchase_method','sale_line_warn',
             'purchase_line_warn','expense_policy','invoice_policy','volume','weight','has_configurable_attributes','create_date','write_date','available_in_pos']
    with open('import/import_product_template.csv', 'w') as f:
        writer = csv.DictWriter(f, fieldnames=labels)
        writer.writeheader()
        for elem in templates:
            writer.writerow(elem) 
    print("fin de generation file csv")
generate_product_template('product.template.csv')
