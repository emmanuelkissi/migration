# -*- coding: utf-8 -*-
import re
import csv
def generate_product_variant_combination(brute_file,first_barcode,nmbre_line,import_product_product_file,import_a_va_file):

    principale_ids = {'Bordeau':552, 'Brique':553, 'Argent Brillant':554, 'Rose Brillant':555, 'Blanc':556, 'Jaune':557, 'Rouge':558, 'Noir':559, 'Marron':560, 'Beige':561, 'Camel':562, 'Gris Foncé':563, 'Argent':564, 'Or':565, 'Rose':566, 'Saumon':567, 'Bleu':568, 'Bleu Foncé':569, 'Fuchsia':570, 'Turquoise':571, 'Mauve':572, 'Orange':573, 'Vert':574, 'Vert Olive':575, 'Gris blanc':576, 'Blanc gris':577, 'Bronze':578, 'Armée':579, 'noi':580, '41':581, '42':582, 'VERT':583, '19':584, 'br':585, 'bleu Fonce':586, 'Gris FoncÃ©':587, '36':588, 'Bleu Jeans':589, '40':590, '43':591, 'rou':592}
    secondaire_ids = {'Bordeau':593, 'Blanc':594, 'Jaune':595, 'Rouge':596, 'Noir':597, 'Marron':598, 'Beige':599, 'Camel':600, 'Gris FoncÃ©':601, 'Argent':602, 'Or':603, 'Rose':604, 'Saumon':605, 'Bleu':606, 'Bleu Foncé':607, 'Fuchsia':608, 'Turquoise':609, 'Mauve':610, 'Orange':611, 'Vert':612, 'Vert Olive':613, 'noi':614}
    pointure_ids ={'41':615, '42':616, '22':617, '19':618, '20':619, '21':620, '23':621, '24':622, '25':623, '26':624, '27':625, '28':626, '29':627, '30':628, '31':629, '32':630, '33':631, '34':632, '35':633, '36':634, '37':635, '38':636, '39':637, '40':638, '43':639, '44':640, '45':641, '46':642, '47':643, '48':644, '49':645, '50':646, '2930':647}
    adult_ids = {'small':648, 'medium':649, 'large':650, 'xl':651, 'xxl':652, 'xxxl':653}
    enfant_ids = {'1 an':654, '2 an':655, '3 an':656, '4 an':657, '5 an':658, '6 an':659, '7 an':660, '8 an':661, '9 an':662, '10 an':663, '11 an':664, '12 an':665, '13 an':666, '14 an':667, '15 an':668, '16 an':669}
    
    #principale_ids = {'Bordeau':1, 'Brique':2, 'Argent Brillant':3, 'Rose Brillant':4, 'Blanc':5, 'Jaune':6, 'Rouge':7, 'Noir':8, 'Marron':9, 'Beige':10, 'Camel':11, 'Gris Foncé':12, 'Argent':13, 'Or':14, 'Rose':15, 'Saumon':16, 'Bleu':17, 'Bleu Foncé':18, 'Fuchsia':19, 'Turquoise':20, 'Mauve':21, 'Orange':22, 'Vert':23, 'Vert Olive':24, 'Gris blanc':25, 'Blanc gris':26, 'Bronze':27, 'Armée':28, 'noi':29, '41':30, '42':31, 'VERT':32, '19':33, 'br':34, 'bleu Fonce':35, 'Gris Fonce':36, '36':37, 'Bleu Jeans':38, '40':39, '43':40, 'rou':41}
    #secondaire_ids = {'Bordeau':42, 'Blanc':43, 'Jaune':44, 'Rouge':45, 'Noir':46, 'Marron':47, 'Beige':48, 'Camel':49, 'Gris Foncé':50, 'Argent':51, 'Or':52, 'Rose':53, 'Saumon':54, 'Bleu':55, 'Bleu Foncé':56, 'Fuchsia':57, 'Turquoise':58, 'Mauve':59, 'Orange':60, 'Vert':61, 'Vert Olive':62, 'noi':63}
    #pointure_ids ={'41':64, '42':65, '22':66, '19':67, '20':68, '21':69, '23':70, '24':71, '25':72, '26':73, '27':74, '28':75, '29':76, '30':77, '31':78, '32':79, '33':80, '34':81, '35':82, '36':83, '37':84, '38':85, '39':86, '40':87, '43':88, '44':89, '45':90, '46':91, '47':92, '48':93, '49':94, '50':95, '2930':96}
    #adult_ids = {'small':97, 'medium':98, 'large':99, 'xl':100, 'xxl':101, 'xxxl':102}
    #enfant_ids = {'1 an':103, '2 an':104, '3 an':105, '4 an':106, '5 an':107, '6 an':108, '7 an':109, '8 an':110, '9 an':111, '10 an':112, '11 an':113, '12 an':114, '13 an':115, '14 an':116, '15 an':117, '16 an':118}
    principale_value =[]
    secondaire_value = []
    pointure_value =[]
    adult_value = []
    enfant_value =[]
    principale = 'Couleur Principale'
    pointure = 'Pointure'
    secondaire= 'Couleur Secondaire'
    adult ='Taille Adult'
    enfant ='Taille enfant'
    compteur =0
    test = first_barcode[0:11]
    value_list=[]
    final_value_list=[]
    
   
    with open(brute_file) as csvfile:
        reader = csv.DictReader(csvfile, delimiter=',')     
        for row in reader: 
            barcode = row['barcode']
            compteur +=1
            if(barcode[0:11]==test and principale in row['product_attribute']):
                valeur = re.search('Principale: (.+?),Pointure', row['product_attribute']).group(1)
                if 'Couleur' in valeur:
                    valeur = re.search('Principale: (.+?),Couleur', row['product_attribute']).group(1)
                if(valeur not in principale_value):
                    principale_value.append(valeur)
            if(barcode[0:11]==test and secondaire in row['product_attribute']):
                valeur = re.search('Secondaire: (.+?),Pointure', row['product_attribute']).group(1)
                if(valeur not in secondaire_value):
                    secondaire_value.append(valeur)
            if(barcode[0:11]==test and pointure in row['product_attribute']):
                valeur = re.search('Pointure: (.+$)', row['product_attribute']).group(1)
                if(valeur not in pointure_value):
                    pointure_value.append(valeur)
            if(barcode[0:11]==test and adult in row['product_attribute']):
                valeur = re.search('): (.+$)', row['product_attribute']).group(1)
                if(valeur not in adult_value):
                    adult_value.append(valeur)
            if(barcode[0:11]==test and enfant in row['product_attribute']):
                valeur = re.search('ts): (.+$)', row['product_attribute']).group(1)
                if(valeur not in enfant_value):
                    enfant_value.append(valeur)
            
            if(barcode[0:11]==test):
                line_value={
                    "name":row['name'],
                    "principale":principale_value,   
                    "secondaire":secondaire_value,
                    "pointure":pointure_value,
                    "adult":adult_value,
                    "enfant":enfant_value,
                    "barcode":barcode        
                }
                value_list.append(line_value)
            if(barcode[0:11]!=test):
                value = value_list[-1]
                final_value_list.append(value)
                principale_value =[]
                secondaire_value = []
                pointure_value =[]
                adult_value = []
                enfant_value =[]
            if(barcode[0:11]!=test and principale in row['product_attribute']):
                valeur = re.search('Principale: (.+?),Pointure', row['product_attribute']).group(1)
                if 'Couleur' in valeur:
                    valeur = re.search('Principale: (.+?),Couleur', row['product_attribute']).group(1)
                if(valeur not in principale_value):
                    principale_value.append(valeur)
            if(barcode[0:11]!=test and secondaire in row['product_attribute']):
                valeur = re.search('Secondaire: (.+?),Pointure', row['product_attribute']).group(1)
                if(valeur not in secondaire_value):
                    secondaire_value.append(valeur)
            if(barcode[0:11]!=test and pointure in row['product_attribute']):
                valeur = re.search('Pointure: (.+$)', row['product_attribute']).group(1)
                if(valeur not in pointure_value):
                    pointure_value.append(valeur)
            if(barcode[0:11]!=test and adult in row['product_attribute']):
                valeur = re.search('): (.+$)', row['product_attribute']).group(1)
                if(valeur not in adult_value):
                    adult_value.append(valeur)
            if(barcode[0:11]!=test and enfant in row['product_attribute']):
                valeur = re.search('ts): (.+$)', row['product_attribute']).group(1)
                if(valeur not in enfant_value):
                    enfant_value.append(valeur)
            if(compteur == nmbre_line) :
                final_value_list.append(value_list[-1])          
            test = barcode[0:11]
        #print("final",final_value_list)
    product_template_attribute_values =[]
    variant_combinations =[]
    with open(import_a_va_file) as csvfile:
        reader = csv.DictReader(csvfile, delimiter=',')     
        for row in reader:
            attribute_value = {
                'id':int(row['id']),
                'product_attribute_value_id':row['product_attribute_value_id'],
                'attribute_id':row['attribute_id'],
                'product_tmpl_id':row['product_tmpl_id']
            }
            product_template_attribute_values.append(attribute_value)
    product_ids =[]
    principale_id=[]
    secondaire_id=[]
    pointure_id=[]
    adult_id=[]
    enfant_id=[]
    with open(import_product_product_file) as csvfile:
        reader = csv.DictReader(csvfile, delimiter=',')     
        for row in reader:
            product_id ={
                'product_id':int(row['id']),
                'product_tmpl_id':int(row['product_tmpl_id'])
            }
            product_ids.append(product_id)
    #print("ids: ",len(product_ids))
    #print("value",len(product_template_attribute_values))
    condition =True
    i1= len(product_ids)
    i3=len(product_template_attribute_values)
    i,i2,t =0,0,0
    use_ids =[]
    #creation de list pour chaque attribute et ensuit iterer pour attribuer les id de product_template_attribute_value
    print('--------------------AVANT',len(product_template_attribute_values))
    existe_combination=[]
    
    while condition:
        product=product_ids[i] #i+1 pour dire on reprend ou on s'est arrêter + 1 si sa fait pas correctement fait i ou i-1
        condition2 =True
        print('-----------product',product)
        for j in range(0,len(product_template_attribute_values)):
            attributes = product_template_attribute_values[j]
            if(product['product_tmpl_id'])==int(attributes['product_tmpl_id']):
                if attributes['attribute_id']=='2' and attributes['id'] not in principale_id:
                    principale_id.append(attributes['id']) 
                if attributes['attribute_id']=='3' and attributes['id'] not in secondaire_id:  
                    secondaire_id.append(attributes['id']) 
                if attributes['attribute_id']=='4' and attributes['id'] not in pointure_id: 
                    pointure_id.append(attributes['id'])
                if attributes['attribute_id']=='5' and attributes['id'] not in adult_id:
                    adult_id.append(attributes['id'])
                if attributes['attribute_id']=='6' and attributes['id'] not in enfant_id:
                    enfant_id.append(attributes['id'])
        print("bon",principale_id)
        print("tes1",pointure_id)
        if len(principale_id)!=0:
            taille = len(principale_id)*len(pointure_id)+(len(secondaire_id)*len(pointure_id))
        if len(adult_id)!=0:
            taille = (len(principale_id)*len(adult_id))+(len(secondaire_id)*len(adult_id))
        if len(enfant_id)!=0:
            taille = (len(principale_id)*len(enfant_id))+(len(secondaire_id)*len(enfant_id))
        
        while condition2:      
            if len(principale_id) !=0:
                for k in range(0,len(principale_id)): 
                    print('-----------------------------011')         
                    if len(pointure_id) !=0:
                        for p in range(0,len(pointure_id)):
                            variant_comination = {
                            'id1':principale_id[k],
                            'id2':pointure_id[p]
                            }
                            variant_combinations.append(variant_comination)
                            i2 +=1
                    if len(adult_id) !=0:
                        print('-----------------------------021')
                        for l in range(0,len(adult_id)):
                            variant_comination = {
                            'id1':principale_id[k],
                            'id2':adult_id[l]
                            }
                            variant_combinations.append(variant_comination)
                            i2 +=1
                    if len(enfant_id) !=0:
                        for n in range(0,len(enfant_id)):
                            variant_comination = {
                            'id1':principale_id[k],
                            'id2':enfant_id[n]
                            }
                            variant_combinations.append(variant_comination)
                            i2 +=1
                   # print('ccccccccccccccccccc',variant_combinations)
            if len(secondaire_id) !=0:
                print('-----------------------------022')
                for k1 in range(0,len(secondaire_id)):        
                    if len(pointure_id) !=0:
                        for p1 in range(0,len(pointure_id)):
                            variant_comination = {
                            'id1':principale_id[k1],
                            'id2':pointure_id[p1]
                            } 
                            variant_combinations.append(variant_comination)
                            i2 +=1
                    if len(adult_id) !=0:
                        print('-----------------------------01')
                        for l1 in range(0,len(adult_id)):
                            variant_comination = {
                            'id1':principale_id[k1],
                            'id2':adult_id[l1]
                            }
                            variant_combinations.append(variant_comination)
                            i2 +=1
                    if len(enfant_id) !=0:
                        print('-----------------------------01')
                        for n1 in range(0,len(enfant_id)):
                            variant_comination = {
                            'id1':principale_id[k1],
                            'id2':enfant_id[n1]
                            }
                            variant_combinations.append(variant_comination)
                            i2 +=1
            if i2> taille-1:                
                condition2=False
        print("------------fin de fort---------")
        print("taille",taille)
        print("principale",principale_id)
        i =i2
        if i> i1-1:
            condition=False  
        #je vide les lists pour reprendre ou il s'est arreter +1
        principale_id=[]
        secondaire_id=[]
        pointure_id=[]
        adult_id=[]
        enfant_id=[] 
    print("taille",taille)
    print("principale",principale_id)
    final_variant_combinations = []
    print('************',(variant_combinations))
    print('+++++++++++',len(product_ids))
    
    for j in range(0,len(variant_combinations)):
        variant_combine = variant_combinations[j]
        print("product id ---------",j)
        
        
        #id_product = product_ids[variant_combine['index_product_id']]
        id_product = product_ids[j]
        dict_variant_combination ={
            'product_product_id':id_product['product_id'],
            'product_template_attribute_value_id':variant_combine['id1']
        }  
        final_variant_combinations.append(dict_variant_combination) 
        dict_variant_combination ={
            'product_product_id':id_product['product_id'],
            'product_template_attribute_value_id':variant_combine['id2']
        } 
        final_variant_combinations.append(dict_variant_combination)
    #print(product_ids)               
    #print(variant_combinations)
    labels =['product_product_id','product_template_attribute_value_id']
    with open('import/import_product_variant_combination.csv', 'w') as f:
        writer = csv.DictWriter(f, fieldnames=labels)
        writer.writeheader()
        for elem in final_variant_combinations:
            writer.writerow(elem) 
    print("fin de generation file csv")                   
                    
  
            
generate_product_variant_combination('test_o15_2/product_product.csv','20210500739410640',60,"import/import_product_product.csv","import/import_product_template_attribute_value.csv")
# toute les id sont des id bd