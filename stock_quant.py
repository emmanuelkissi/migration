# -*- coding: utf-8 -*-
import  csv
import re
import psycopg2
import sys
import csv
import glob
import pandas as pd
from datetime import datetime

def generate_stock_file(location_extra_file,product_variant_file):
    list_variant=[]
    quant_list =[]
    with open(product_variant_file) as csvfile:
        reader = csv.DictReader(csvfile, delimiter=',')     
        for row in reader:
            variant ={
                "barcode":row['barcode'],
                "name":row['name'],
                "product_attribute":row['product_attribute']           
            }
            list_variant.append(variant) 
    with open(location_extra_file) as csvfile:
        reader = csv.DictReader(csvfile, delimiter=',')     
        for row in reader:
            article = row['product_id']
            valeur1 = re.search("\((.*),", article).group(1)
            valeur2 = re.search(",(.*)\)", article).group(1)
            name = re.search('(^.+?) \(', article).group(1)
            #\ ('(.*)',(.*)\)
            #print(type(name))
            #print(valeur1)
            #print(valeur2)
            for i in range(0,len(list_variant)):
                product = list_variant[i]
                product_name = str(product['name'])
                attribute = str(product['product_attribute'])
                if product_name==name:
                    if valeur1 in attribute and valeur2 in attribute:
                        quant ={
                            'location_id':row['location_id'],
                            'quantity':row['quantity'],
                            'reserved_quantity':0,
                            'company_id':1,
                            'barcode':product['barcode']
                        }
                        quant_list.append(quant)
    labels =['location_id','quantity','reserved_quantity','company_id','barcode']
    with open('import/stock_quant_file.csv', 'w') as f:
        writer = csv.DictWriter(f, fieldnames=labels)
        writer.writeheader()
        for elem in quant_list:
            writer.writerow(elem) 
            
     
    print("fin de l'action !")
    #valeur = re.search('Principale: (.+?),Pointure', row['product_attribute']).group(1)
    return
def generate_stock_quant_import(product_barcode_id_file,stock_quant_file):
    barcode_product_id=[]
    stock_quants =[]
    with open(product_barcode_id_file) as csvfile:
        reader = csv.DictReader(csvfile, delimiter=',')     
        for row in reader:
            barcode_product ={
                "barcode":row['barcode'],
                "id":row['id']           
            }
            barcode_product_id.append(barcode_product) 
    with open(stock_quant_file) as csvfile:
        reader = csv.DictReader(csvfile, delimiter=',')     
        for row in reader:
            barcode1 = row['barcode']
            for i in range(0,len(barcode_product_id)):
                product_barcode = barcode_product_id[i]
                barcode2 = product_barcode['barcode']
                if barcode1==barcode2:
                    stock_quant ={
                        'location_id':row['location_id'],
                        'quantity':row['quantity'],
                        'reserved_quantity':0,
                        'company_id':1,
                        'product_id':product_barcode['id'],
                        'in_date':datetime.now()
                    }
                    stock_quants.append(stock_quant)
    labels =['location_id','quantity','reserved_quantity','company_id','product_id','in_date']
    with open('import/import_stock_quant.csv', 'w') as f:
        writer = csv.DictWriter(f, fieldnames=labels)
        writer.writeheader()
        for elem in stock_quants:
            writer.writerow(elem)
    print("import stock quant genere !")
def get_product_id_barcode():
    try:
        conn = psycopg2.connect(
            user = "odoo",
            password = "12345678",
            host = "localhost",
            port = "5432",
            database = "migration-test4"
        )
        cur = conn.cursor()

        sql = "SELECT id,barcode  FROM product_product WHERE id>2"

        cur.execute(sql)
        print("Sélectionner des lignes dans la table template")
        res = cur.fetchall() 
        fileName = ["id","barcode"]
        with open("file/product_id_barcode.csv", 'w') as csvfile:
            writer = csv.writer(csvfile)
            writer.writerow(fileName)
            for row in res:
                print("Id = ", row[0], )
                writer.writerow([row[0],row[1]])
    
        #fermeture de la connexion à la base de données
        cur.close()
        conn.close()
        print("La connexion PostgreSQL est fermée")

    except (Exception, psycopg2.Error) as error :
        print ("Erreur lors du sélection à partir de la table person", error)

def fusion_file():
    path =r'stock'
    file_identifier = "*.xlsx"
    all_data = pd.DataFrame()
    for f in glob.glob(path + "/*" + file_identifier):
        df = pd.read_excel(f)
        all_data = all_data.append(df,ignore_index=True)
    all_data.to_csv("file/fusion_stock_quant.csv")
#get_product_id_barcode()
#generate_stock_file("test_o15_2/stock_quant.csv","test_o15_2/product_product.csv")
generate_stock_quant_import("import/import_product_product.csv","import/stock_quant_file.csv")
#fusion_file()